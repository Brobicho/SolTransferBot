# SolTransferBot

## Installation

1. ```pip install solana solders```

2. Open file "transfer.py" and input WALLET_SOURCE, WALLET_DEST, WALLET_PRIVATE_KEY (and WALLET_DEST_PRIVATE_KEY if you want to use the "reverse" option)

## Usage

```python3 transfer.py <SOL amount>```

Ex.: Transfer 0.5 SOL from WALLET_SOURCE to WALLET_DEST

```python3 transfer.py 0.5```


You can also transfer from the dest wallet to the source wallet:

```python3 transfer.py <SOL amount> reverse```
