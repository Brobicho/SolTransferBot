import datetime
import sys
from solana.rpc.api import Client
from solders.pubkey import Pubkey
from solders.keypair import Keypair
from solana.transaction import Transaction
import solders.system_program as sp
from solana.rpc.types import TxOpts
from solana.rpc.commitment import Processed
import solders.compute_budget as cb

WALLET_PRIVATE_KEY = ''
WALLET_SOURCE = ''
WALLET_DEST=''
WALLET_DEST_PRIVATE_KEY = ''
ENDPOINT = ""

def fancyPrint(amount):
    return str(round(int(amount) / 1000000000, 2))

def get_client(endpoint):
    return Client(endpoint, commitment=Processed)

def get_balance(client, pubkey):
    return client.get_balance(pubkey)

def sendSol(client, fromKeypair, toWalletPubkey, amount):
    balance = get_balance(client, fromKeypair.pubkey()).value
    unitLimit = 3000
    unitPrice = 200
    coverfee = 5000
    sendMax = False
  
    if balance < coverfee:
      print('balance is too low, can\'t cover fees (', fancyPrint(balance), 'SOL)')
      sys.exit()
    if balance < amount + coverfee:
      print('missing', fancyPrint((amount + coverfee) - balance), 'SOL, sending', fancyPrint(balance - coverfee), 'SOL instead')
      amount = (balance - coverfee)
      sendMax = True
      print('amount:', amount)
    print('sending', fancyPrint(amount), 'SOL to:', toWalletPubkey, 'at:', datetime.datetime.now())
    transfer_tx = Transaction().add(
        sp.transfer(
            sp.TransferParams(
                from_pubkey=fromKeypair.pubkey(), to_pubkey=toWalletPubkey, lamports=amount
            )
        )
      )
    if sendMax == False:
      transfer_tx.add(
        cb.set_compute_unit_limit(unitLimit)
      ).add(
        cb.set_compute_unit_price(unitPrice)
      )

    last_valid_block_height = client.get_latest_blockhash().value.last_valid_block_height
    response = client.send_transaction(transfer_tx, fromKeypair, opts=TxOpts(skip_confirmation=False, skip_preflight=True, preflight_commitment=Processed, max_retries=1, last_valid_block_height=last_valid_block_height))
    print(f"Transaction response: {response} at {datetime.datetime.now()}")

def confirm_transaction(client, signature):
    confirmed = False
    while not confirmed:
        result = client.confirm_transaction(signature)
        if result:
            confirmed = True
        else:
            print(f"Waiting for confirmation of transaction {signature}...")
    print(f"Transaction {signature} confirmed at {datetime.datetime.now()}")

if len(sys.argv) > 2 and sys.argv[2] == 'reverse':
  WALLET_DEST = WALLET_SOURCE
  WALLET_PRIVATE_KEY = WALLET_DEST_PRIVATE_KEY
client = get_client(ENDPOINT)
pubkeyDest = Pubkey.from_string(WALLET_DEST)
fromKeypair = Keypair.from_base58_string(WALLET_PRIVATE_KEY)
amount = int(float(sys.argv[1]) * 10 ** 9)
sendSol(client, fromKeypair, pubkeyDest, amount)
